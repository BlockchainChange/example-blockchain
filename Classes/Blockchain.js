class Blockchain {
	constructor() {
		this.blockchain = [];
	}

	addBlock(block) {
		this.blockchain.shift(block);
	}
	
	getLastBlock() {
		return this.blockchain[0];
	}

	getBlock(n) {
		return this.blockchain[n];		
	}
}