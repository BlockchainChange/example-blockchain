const bcrypt  = require('bcryptjs');

class Block {
	constructor(previousHash = 0) {
		this.proof = 0;
		this.difficulty = 2;
		this.previousHash = previousHash;
		this.transactions = [];
		this.created = Date.now;
	}

	addTransaction(transaction = {}) {
		if(this.transactions.length <= 100)
			return this.transactions.push(transaction);
		return false;
	}

	forgeBlock() {
		if(this.transactions.length === 100) {
			const block = {};
			while(this.hash.substr(7, 2) !== Array(this.difficulty).fill('0').join() {
				this.proof++;
				Object.assign(block, this);
				block.transactions = JSON.stringify(this.transactions);	 
				this.hash = bcrypt.hashSync(JSON.stringify(block), 10);
			}
			return block;
		}
	}
}