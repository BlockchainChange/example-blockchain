class Transaction {
	constructor(sender, receiver, amount = 0, message = undefined) {
		this.sender = sender;
		this.receiver = receiver;
		this.trx = { amount, message };
		this.created = Date.now();
	}
}