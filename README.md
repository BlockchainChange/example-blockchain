## Example Blockchain ##
** BlockchainChange.org **
	NOTE :  For Educational Purposes Only

This project is an open source project developed to help others learn about blockchain, and how the technology can help change the way we do business.

In this repo you will find the basic building blocks to a very simple centralized blockchain.

If you would like to contribute to this or any of the Blockchain Change project's we ask you follow this process.

	Make a Fork
	Create A Branch -- Keep it simple and to the point of the change
		Include an Issue Number if exists.
	Make your changes and save them!
	Create A Pull Request 
		Include an Issue Number and a description of what your branch does.
		Tell others to test your branch so your can get a confirmation!
	
Once a pull request has been approved for merge, you will be sent a message.
You will need to include a wallet address in the comments of your pull request in order to be elegiable to receive any bounties, while bounties are available.